﻿using UnityEngine;
using System.Collections;

public class UIMainMenu : MonoBehaviour {
    public void Play()
    {
        GameObject m = GameObject.Find("GameManager");
        m.GetComponent<ScreenManager>().LoadLevel("Map", "");
    }
    public void Exit()
    {
        Application.Quit();
    }
}
